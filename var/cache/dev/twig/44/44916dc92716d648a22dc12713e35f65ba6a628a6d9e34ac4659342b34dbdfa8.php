<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_b2863a3447add9624b268b079803e732c314fb099204949cad41b45f412f1e6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48ae92a525f6539c00751e146c839fe4d63b39e6cb1bb522dfe761be67be3115 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48ae92a525f6539c00751e146c839fe4d63b39e6cb1bb522dfe761be67be3115->enter($__internal_48ae92a525f6539c00751e146c839fe4d63b39e6cb1bb522dfe761be67be3115_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        $__internal_053e887cbb562f108779cde00e436181b2ced0f0cee5ed9ca9e2bfd332cdef64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_053e887cbb562f108779cde00e436181b2ced0f0cee5ed9ca9e2bfd332cdef64->enter($__internal_053e887cbb562f108779cde00e436181b2ced0f0cee5ed9ca9e2bfd332cdef64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_48ae92a525f6539c00751e146c839fe4d63b39e6cb1bb522dfe761be67be3115->leave($__internal_48ae92a525f6539c00751e146c839fe4d63b39e6cb1bb522dfe761be67be3115_prof);

        
        $__internal_053e887cbb562f108779cde00e436181b2ced0f0cee5ed9ca9e2bfd332cdef64->leave($__internal_053e887cbb562f108779cde00e436181b2ced0f0cee5ed9ca9e2bfd332cdef64_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 4,  31 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "/var/www/html/opensilogBooks/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}
