<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_0b0718acc30ca6197bc8c384956f818e1249d09e7c1b75592639dbed99f0487d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78e591b7fd7e7241d6daedf3e3be4cd4330335c33ced347f9b56fa54c6aec75f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78e591b7fd7e7241d6daedf3e3be4cd4330335c33ced347f9b56fa54c6aec75f->enter($__internal_78e591b7fd7e7241d6daedf3e3be4cd4330335c33ced347f9b56fa54c6aec75f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_232ba15bf5aa54cd228b9a2d7d19b5c5e88643342141adfa990dc7280c7c226a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_232ba15bf5aa54cd228b9a2d7d19b5c5e88643342141adfa990dc7280c7c226a->enter($__internal_232ba15bf5aa54cd228b9a2d7d19b5c5e88643342141adfa990dc7280c7c226a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78e591b7fd7e7241d6daedf3e3be4cd4330335c33ced347f9b56fa54c6aec75f->leave($__internal_78e591b7fd7e7241d6daedf3e3be4cd4330335c33ced347f9b56fa54c6aec75f_prof);

        
        $__internal_232ba15bf5aa54cd228b9a2d7d19b5c5e88643342141adfa990dc7280c7c226a->leave($__internal_232ba15bf5aa54cd228b9a2d7d19b5c5e88643342141adfa990dc7280c7c226a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b49e1a171a9649c33a370d383c8e53e9127f2430c59f0cb126d399fc86cdc35a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b49e1a171a9649c33a370d383c8e53e9127f2430c59f0cb126d399fc86cdc35a->enter($__internal_b49e1a171a9649c33a370d383c8e53e9127f2430c59f0cb126d399fc86cdc35a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_4369c7858ae36498a36773bbb6b989af1c897464da3675315afc90dd0a1ecc96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4369c7858ae36498a36773bbb6b989af1c897464da3675315afc90dd0a1ecc96->enter($__internal_4369c7858ae36498a36773bbb6b989af1c897464da3675315afc90dd0a1ecc96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_4369c7858ae36498a36773bbb6b989af1c897464da3675315afc90dd0a1ecc96->leave($__internal_4369c7858ae36498a36773bbb6b989af1c897464da3675315afc90dd0a1ecc96_prof);

        
        $__internal_b49e1a171a9649c33a370d383c8e53e9127f2430c59f0cb126d399fc86cdc35a->leave($__internal_b49e1a171a9649c33a370d383c8e53e9127f2430c59f0cb126d399fc86cdc35a_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_dfa4a35ad79828402c40408a6fc96f25eb6ee1a0f0efba0ef18a51de97de39fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfa4a35ad79828402c40408a6fc96f25eb6ee1a0f0efba0ef18a51de97de39fa->enter($__internal_dfa4a35ad79828402c40408a6fc96f25eb6ee1a0f0efba0ef18a51de97de39fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_28a663b6512f68689aee673bbcb966721e63bc1633565a6ac31b2da1d0d1d843 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28a663b6512f68689aee673bbcb966721e63bc1633565a6ac31b2da1d0d1d843->enter($__internal_28a663b6512f68689aee673bbcb966721e63bc1633565a6ac31b2da1d0d1d843_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_28a663b6512f68689aee673bbcb966721e63bc1633565a6ac31b2da1d0d1d843->leave($__internal_28a663b6512f68689aee673bbcb966721e63bc1633565a6ac31b2da1d0d1d843_prof);

        
        $__internal_dfa4a35ad79828402c40408a6fc96f25eb6ee1a0f0efba0ef18a51de97de39fa->leave($__internal_dfa4a35ad79828402c40408a6fc96f25eb6ee1a0f0efba0ef18a51de97de39fa_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_5bafa07bc271c1ed211c9a077e1590dc8ac6bcc862c2829acd23e5c0120fa822 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bafa07bc271c1ed211c9a077e1590dc8ac6bcc862c2829acd23e5c0120fa822->enter($__internal_5bafa07bc271c1ed211c9a077e1590dc8ac6bcc862c2829acd23e5c0120fa822_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_f2a8f93a02747b7ba8e4edc4214169076105ffd98f284ca0084c5af602f0a738 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2a8f93a02747b7ba8e4edc4214169076105ffd98f284ca0084c5af602f0a738->enter($__internal_f2a8f93a02747b7ba8e4edc4214169076105ffd98f284ca0084c5af602f0a738_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_f2a8f93a02747b7ba8e4edc4214169076105ffd98f284ca0084c5af602f0a738->leave($__internal_f2a8f93a02747b7ba8e4edc4214169076105ffd98f284ca0084c5af602f0a738_prof);

        
        $__internal_5bafa07bc271c1ed211c9a077e1590dc8ac6bcc862c2829acd23e5c0120fa822->leave($__internal_5bafa07bc271c1ed211c9a077e1590dc8ac6bcc862c2829acd23e5c0120fa822_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/var/www/html/opensilogBooks/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
