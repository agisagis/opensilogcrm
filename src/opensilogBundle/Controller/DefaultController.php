<?php

namespace opensilogBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('opensilogBundle:Default:index.html.twig');
    }}
